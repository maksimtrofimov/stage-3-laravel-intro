<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Cheap products</title>
</head>
<body>
    List of cheap products
    <table>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Price</th>
            <th>Image</th>
            <th>Rating</th>
        <tr>
        @foreach($products as $product)
            <tr>
                <td>{{ $product->getId() }}</td>
                <td>{{ $product->getName() }}</td>
                <td>{{ $product->getPrice() }}</td>
                <td>{{ $product->getImageUrl() }}</td>
                <td>{{ $product->getRating() }}</td>
            <tr>
        @endforeach
    <table>
</body>
</html>