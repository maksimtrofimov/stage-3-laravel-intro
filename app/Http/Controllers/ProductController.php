<?php

namespace App\Http\Controllers;

use App;
use App\Action\Product\GetMostPopularProductAction;
use App\Action\Product\GetAllProductsAction;
use App\Http\Presenter\ProductArrayPresenter;
use Illuminate\Http\JsonResponse;
use App\Action\Product\GetCheapestProductsAction;

class ProductController extends Controller
{
    public function getPopular()
    {
        $action = App::make(GetMostPopularProductAction::class);
        $response = $action->execute();
        $product = $response->getProduct();
        $product = ProductArrayPresenter::present($product);
        return new JsonResponse($product);
    }

    public function getProducts()
    {
        $action = App::make(GetAllProductsAction::class);
        $response = $action->execute();
        $products = $response->getProducts();
        $products = ProductArrayPresenter::presentCollection($products);
        return new JsonResponse($products);
    }


    public function getCheap()
    {
        $action = App::make(GetCheapestProductsAction::class);
        $response = $action->execute();
        $products = $response->getProducts();
        return view('cheap_products', ['products' => $products]);
    }
}