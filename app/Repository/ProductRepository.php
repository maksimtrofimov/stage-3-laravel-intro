<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Product;

class ProductRepository implements ProductRepositoryInterface
{
	protected $products;
	/**
     * @param Product[] $products
     */
	public function __construct(array $products)
	{
		$this->products = $products;
	}

    /**
     * @return Product[]
     */
	public function findAll(): array
	{
		return $this->products;
	}

	public function findThreeCheapest(): array
	{
		$products = $this->products;
		if (count($products) >= 3) {
			usort($products, function($a, $b){
				return $a->getPrice() <=> $b->getPrice();
			});
			return [$products[0], $products[1], $products[2]];
		} else {
			throw new \Exception(__METHOD__ . ' The product array must contain at least three arrays');
		}
	}

	public function popular(): Product
	{
		$products = $this->products;
		if (!empty($products)) {
			usort($products, function($a, $b){
				return $b->getRating() <=> $a->getRating();
			});
			return $products[0];
		} else {
			throw new \Exception(__METHOD__ . ' The product array is empty');
		}
	}
}
