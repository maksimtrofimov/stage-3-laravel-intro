<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Action\Product\GetAllProductsResponse;
use App\Repository\ProductRepositoryInterface;

class GetCheapestProductsAction
{
    private $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function execute(): GetCheapestProductsResponse
    {
        return new GetCheapestProductsResponse($this->repository);
    }
}