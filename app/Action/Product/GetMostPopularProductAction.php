<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;
use App\Action\Product\GetMostPopularProductResponse;

class GetMostPopularProductAction
{
    private $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function execute(): GetMostPopularProductResponse
    {
        return new GetMostPopularProductResponse($this->repository);
    }
}