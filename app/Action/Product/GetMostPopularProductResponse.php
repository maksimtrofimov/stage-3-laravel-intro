<?php

declare(strict_types=1);

namespace App\Action\Product;

class GetMostPopularProductResponse
{
    private $products;

    public function __construct($products)
    {
        $this->products = $products;
    }

    public function getProduct()
    {
        return $this->products->popular();
    }
}